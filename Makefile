ALL: packages comp doxygen test

packages:
	@echo "**************************************************************************"
	@echo "** Se instalarán los paquetes necesarios para que funciones el programa **"
	@echo "**************************************************************************"
	@sleep 2s

	sudo apt install doxygen
	sudo apt install doxygen-gui
	sudo apt-get install graphviz
	sudo apt install texlive-latex-extra
	sudo apt install texlive-lang-spanish

comp:
	@echo "**************************************************************************"
	@echo "***********           Se generará el ejecutable            ***************"
	@echo "**************************************************************************"
	@sleep 2s

	mkdir -p bin build docs

	g++ -o ./build/Fraccion.o -c ./src/Fraccion.cpp
	g++ -o ./build/Polinomio.o -c ./src/Polinomio.cpp
	g++ -o ./build/Verificacion_parametros_variables.o -c ./src/Verificacion_parametros_variables.cpp
	g++ -o ./build/Control.o -c ./src/Control.cpp
	g++ -o ./build/main.o -c ./src/main.cpp

	g++ -o ./bin/lab3 ./build/main.o ./build/Verificacion_parametros_variables.o ./build/Control.o ./build/Polinomio.o ./build/Fraccion.o

doxygen:
	@echo "**************************************************************************"
	@echo "************      Generando el archivo refman.pdf          ***************"
	@echo "**************************************************************************"
	@sleep 2s

	mkdir -p docs
	doxygen Doxyfile
	make -C ./docs/latex


test:
	@echo "**************************************************************************"
	@echo "***********         Pruebas para Fracción                    *************"
	@echo "**************************************************************************"
	@sleep 2s
	@echo
	@echo Suma
	@./bin/lab3 1 / 2 + 2 / 3
	@echo

	@echo Resta
	@./bin/lab3 1 / 2 - 2 / 3
	@echo

	@echo Multiplicación
	@./bin/lab3 1 / 2 '*' 2 / 3
	@echo

	@echo División
	@./bin/lab3 1 / 2 / 2 / 3
	@echo

	@echo "**************************************************************************"
	@echo "************         Pruebas para Polinomio                  *************"
	@echo "**************************************************************************"
	@sleep 2s

	@echo
	@echo Suma
	@./bin/lab3 1 2 3 + 4 5 6
	@echo

	@echo Resta
	@./bin/lab3 1 2 3 - 4 5 6
	@echo

	@echo Multiplicación
	@./bin/lab3 1 2 3 '*' 4 5 6
	@echo

	@echo División
	@./bin/lab3 1 2 3 / 4 5 6
	@echo