#pragma once

/**
 * @date 22/01/2020
 * 
 * @file Control.h
 * @brief Aqui se define la estructura de la clase Control.
 */

#include <iostream>
#include "../include/Fraccion.h"
#include "../include/Polinomio.h"
#include "../include/Verificacion_parametros_variables.h"

using namespace std;


/**
 *  @class Control
 *  @brief Maneja el flujo de control del programa.
 */
class Control
{
    public:

        /**
         * @brief Constructor que maneja toda la lógica del programa.
         * 
         * @param ARG Número de argumentos dados por el usuario.
         * @param ARGV Argumentos pasados por el usuario.
         */
        Control( int ARG, char **ARGV );


        /**
         * @brief Destructor.
         */
        ~Control();
};