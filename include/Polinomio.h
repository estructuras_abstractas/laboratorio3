#pragma once

/**
 * @date 22/01/2020
 * 
 * @file Polinomio.h
 * @brief Aqui se define la estructura de la clase Polinomio.
 */

#include <iostream>
#include <stdlib.h>
#include <string>

using namespace std;

/**
 * @class Polinomio
 * @brief Incluye elementos privados encargados de almacenar
 * los datos correspondientes al polinomio ingresado (A B C), el cociente, el residuo y las banderas
 * mul_valida y division_valida que indican si se está multiplicando o dividiendo.
 */
class Polinomio
{
    public:

        /**
         * @brief Constructor, inicializa el polinomio.
         * 
         * @param X Primer elemento.
         * @param Y Segundo elemento.
         * @param Z Tercer elemento.
         */
        Polinomio(int X, int Y, int Z);
        
        
        /**
         * @brief Destructor.
         */
        ~Polinomio();


        /**
         * @brief Sobrecarga del ooperador +
         * 
         * @param obj Objeto tipo polinomio.
         */
        void operator+(const Polinomio &obj);


        /**
         * @brief Sobrecarga del ooperador -
         * 
         * @param obj Objeto tipo polinomio.
         */
        void operator-(const Polinomio &obj);


        /**
         * @brief Sobrecarga del ooperador *
         * 
         * @param obj Objeto tipo polinomio.
         */
        void operator*(const Polinomio &obj);

    
        /**
         * @brief Sobrecarga del ooperador /
         * 
         * @param obj Objeto tipo polinomio.
         */
        void operator/(const Polinomio &obj);
        

        /**
         * @brief Sobrecarga del ooperador <<
         * 
         * @param os Objeto de salida, imprime en la terminal.
         * @param obj Objeto tipo polinomio por referencia.
         */
        friend ostream& operator<< ( ostream &os, const Polinomio &obj );


        /**
         * @brief Sobrecarga del ooperador >>
         * 
         * @param os Objeto de entrada, recibe los parámetros del otro polinomio.
         * @param obj Objeto tipo polinomio por referencia.
         */
        friend istream& operator>> ( istream &is, Polinomio &obj );
    
    private:
        int x;
        int y;
        int z;

        float x1, x2, x3;

        float a, b, c, d, e;

        float cociente;
        float residuo[2];

        bool mul_valida; //< ïndica si lo que se va a hacer es una multiplicación.
        unsigned short int division_valida; //< Indica si la divición es válida o no.
};