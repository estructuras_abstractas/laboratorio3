#pragma once

/**
 * @date 22/01/2020
 *
 * @file Verificacion_parametros_variables.h
 * @brief En este archivo se definen los métodos de la clase Verificacion_parametros_variables.
 */

#include <iostream>
#include <string>
#include <stdlib.h> //< Incluye la función atof


using namespace std;

/**
 * @class Verificacion_parametros_variables
 * @brief Controla que los parámetros introducidos por el usuario sean los correctos, también determina
 * si se está haciendo una fracción o un polinomio.
 */
class Verificacion_parametros_variables
{
    public:

        /**
         * @brief Constructor por defecto, no cumple ninguna función.
         */
        Verificacion_parametros_variables ();

        /**
         * @brief Destructor.
         */
        ~Verificacion_parametros_variables ();


        /**
         * @brief Esta función determina si los números introducidos por el usuario son
         * enteros o reales, si son reales devuelve 0, si son enteros regresa 1. 
         * 
         * @param A Número de argumentos.
         * @param C Los argumentos.
         * @return Cero si es un float, 1 si es un entero y 2 si hay un error.
         */
        unsigned short int Verifica_int_float ( int A, char **C );


        /**
         * @brief Este método verifica que se hayan introducido números y signos de operación válidos,
         * en este caso regresa 2, si algún elemento no es correcto devuelve 0, si se detecta que hay
         * un 0 en algún divisor regresa 1.
         * 
         * @param A Número de argumentos.
         * @param C Los argumentos.
         * @return 2 si los signos de operación son válidos, 0 si hay un elemento incorrecto y si hay algún 
         * divisor igual a 0 regresa 1.
         */
        unsigned short int Verifica_introdujeron_numeros ( int A, char **C );
};