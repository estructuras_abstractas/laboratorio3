#pragma once

/**
 * @date 22/01/2020
 * 
 * @file Fraccion.h
 * @brief En este archivo se definen los métodos de la clase Fraccion.
 */

#include <iostream>
#include <string>

using namespace std;

/**
 * @class Fraccion
 * @brief La clase Fracción define la estructura de los métodos para operar con fracciones.
 */
class Fraccion
{
    public:

        /**
         * @brief Constructor por defecto que recibe los parámetros del usuario.
         * 
         * @param X Primer parámetro.
         * @param Y Segundo parámetro.
         */
        Fraccion(int, int);


        /**
         * @brief Destructor.
         */    
        ~Fraccion();
     

        /**
         * @brief Sobrecarga del ooperador +
         * 
         * @param obj Objeto tipo fracción.
         */
        void operator+(const Fraccion &obj);


        /**
         * @brief Sobrecarga del ooperador -
         * 
         * @param obj Objeto tipo fracción.
         */
        void operator-(const Fraccion &obj);
        

        /**
         * @brief Sobrecarga del ooperador *
         * 
         * @param obj Objeto tipo fracción.
         */ 
        void operator*(const Fraccion &obj);
        
        
        /**
         * @brief Sobrecarga del ooperador /
         * 
         * @param obj Objeto tipo fracción.
         */
        void operator/(const Fraccion &obj);
        

        /**
         * @brief Sobrecarga del ooperador <<
         * 
         * @param os Objeto de salida, imprime en la terminal.
         * @param obj Objeto tipo fracción por referencia.
         */
        friend ostream& operator << ( ostream& os, const Fraccion& obj );
 
 
        /**
         * @brief Recibe los parámetros de la otra fracción.
         * 
         * @param is Objeto de entrada, guarda las variables de la segunda fracción.
         * @param obj Objeto tipo fracción por referencia.
         */
        friend istream& operator >> ( istream& is, Fraccion& obj );
    
    private:
       
        float resultado; //< Resultados de cada operación.

        int x; //< Primer parámetro de la fracción.
        int y; //< Segundo parámetro de la fracción.
};