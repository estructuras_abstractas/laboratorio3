# Laboratorio 3: Sobrecarga de operadores en C++


## Integrante
```
Jorge Muñoz Taylor
```

## Como compilar el proyecto
Una vez descargado el código, ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio3
```
Por último ejecute el make:
```
>>make
```

## Ejecutar el programa
Para polinomios:
```
./bin/lab3 # # # operacion # # #
```

Para fracciones:
```
./bin/lab3 # / # operacion # / # 
```

Donde operación se refiere a suma, resta, multiplicación y división.
El símbolo numeral se refiere a cualquier número entero.

Tenga en cuenta que para el caso de la multiplicación el asterisco se debe escribir '*' de lo contrario no funcionará.

## Para ver los test

Escribir en la terminar:
```
>>make test
```

## PARA Doxygen
Ubíquese en la carpeta donde está el código:
```
>>cd {PATH}/laboratorio3
```

Para compilar el doxygen:
```
>>doxygen Doxyfile
```

Para generar el archivo PDF:
```
>>cd DOCS/latex
>>make
```
Esto generará el archivo PDF con la documentación generada por doxygen. Para verlo:
```
Doble clic al archivo refman.pdf dentro de DOCS/latex
```

## Dependencias
Debe tener instalado CMAKE y MAKE en la máquina:
```
>>sudo apt-get install build-essential
>>sudo apt-get install cmake
```
También doxygen y latex:
```
>>sudo apt install doxygen
>>sudo apt install doxygen-gui
>>sudo apt-get install graphviz
>>sudo apt install texlive-latex-extra
>>sudo apt install texlive-lang-spanish
```