/**
 * @date 22/01/2020
 *
 * @file Polinomio.cpp
 * @brief Se definen los métodos de la clase Polinomio.
 */

#include "../include/Polinomio.h"


Polinomio::Polinomio(int X, int Y, int Z){
    x = X;
    y = Y;
    z = Z;

    mul_valida = false; ///< Dice si se hizo una multiplicación.
    division_valida = 0; ///< Indica si se hizo una división.
    return;
}


Polinomio::~Polinomio(){}


void Polinomio::operator+(const Polinomio &obj){
    
    x1 = (float)x + (float)obj.x;

    x2 = (float)y + (float)obj.y;
    
    x3 = (float)z + (float)obj.z;

    return;
}


void Polinomio::operator-(const Polinomio &obj){

    x1 = (float)x - (float)obj.x;

    x2 = (float)y - (float)obj.y;
    
    x3 = (float)z - (float)obj.z;
    
    return;
}


void Polinomio::operator*(const Polinomio &obj){

    mul_valida = true;

    a = (float)x * (float)obj.x;
    b = (float)x * (float)obj.y + (float)y * (float)obj.x;
    c = (float)x * (float)obj.z + (float)y * (float)obj.y + (float)z * (float)obj.x;
    d = (float)y * (float)obj.z + (float)z * (float)obj.y;
    e = (float)z * (float)obj.z;

    return;
}


void Polinomio::operator/(const Polinomio &obj){

    int dividendo[3];
    int divisor[3];

    dividendo[0] = z;
    dividendo[1] = y;
    dividendo[2] = x;
    divisor[0] = obj.z;
    divisor[1] = obj.y;
    divisor[2] = obj.x;

    if     ( x == 0 && obj.x != 0) division_valida = 2; ///< Grado del dividendo debe ser mayor.
    else if( x == 0 && y == 0 && z == 0) division_valida = 3; ///< Cociente nulo.
    else if( x == 0 && obj.x == 0 && y == 0 && obj.y != 0) division_valida = 2; ////< Grado del dividendo debe ser mayor.
    else if( obj.x == 0 && obj.y == 0 && obj.z == 0) division_valida = 4; ///< Division por cero.
    else{
        division_valida = 1;

        for(int i = 0; i < 3; i++)
        {
            if(dividendo[2-i] != 0)
            {
                for(int j = 0; j < 3; j++)
                {
                    if(dividendo[2-j] != 0)
                    {
                        cociente = dividendo[2-i];
                        cociente = cociente / divisor[2-j];
                        j = 3;
                    }
                
                }//Fin de for
                i = 3;
            }//Fin de if
        }//Fin de for

        residuo[1] = dividendo[1] - cociente*divisor[1];
        residuo[0] = dividendo[0] - cociente*divisor[0];

    } //Fin de else
    return;
}


ostream& operator<< ( ostream &os, const Polinomio &obj )
{
    string coci;
    string rest;
    string x1;
    string simbolo;
    string resi1; 
    string resi0;
    string coci_string;

    if ( obj.mul_valida == true )
    {
        os << obj.a << "x^4 + " << obj.b << "x^3 + " << obj.c << "x^2 + " << obj.d << "x + " << obj.e; 
    }
    else if ( obj.division_valida == 1 )
    {
        coci = "Cociente: ";
        rest = "   | Residuo: ";
        x1 = "x ";
        resi1 = to_string(obj.residuo[1]);
        resi0 = to_string(labs(obj.residuo[0]));
        coci_string = to_string(obj.cociente);

        if(obj.residuo[0] >= 0) simbolo = " + ";
        else simbolo = " - ";
    }
    else if ( obj.division_valida == 2 )
    {
        os << "El grado del polinomio-dividendo debe ser igual o mayor al grado del divisor :V";
    }
    else if ( obj.division_valida == 3 )
    {
        os << "El polinomio-dividendo no puede ser nulo :V";
    }
    else if ( obj.division_valida == 4 )
    {
        os << "El polinomio-divisor no puede ser nulo :V";
    }
    else
    {
        os << "Resultado: " << obj.x1 << "x^2 + " << obj.x2 << "x + " << obj.x3;
    }

    os << coci + coci_string + rest + resi1 + x1 + simbolo + resi0; 
    
    return os;
}


istream& operator>> ( istream &is, Polinomio &obj )
{
    is >> obj.x >> obj.y >> obj.z;
    
    return is;
}