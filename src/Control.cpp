/**
 * @date 22/01/2020
 * 
 * @file Control.cpp
 * @brief Aqui se definen los métodos de la clase Control.
 */

#include "../include/Control.h"


Control::Control(int ARG, char **ARGV ){ ///< Los argumentos se pasan por la terminal, ver README.

    ///< Declaracion de objetos y variables.
    Verificacion_parametros_variables verificar;
    Fraccion* frac;
    Fraccion* frac2;
    Polinomio* pol1;
    Polinomio* pol2;
    
    float x1 , x12 , x13;
    float y1 , y12 , y13;
    float x2, y2, w2, z2;

    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;

    /** Verifica si se introdujo algún parámetro y se identifíca el tipo de función que se va a usar
     * , fracción o polinomio.
     */
    if(ARGV[1] != NULL){

        if( verificar.Verifica_introdujeron_numeros(ARG, ARGV) == 0 ) {
            cout << endl << "Revise los parametros que introdujo, recuerde que la multiplicacion" << endl;
            cout << "debe escribirse asi '*', ademas este programa resuelve unicamente polinomios de";
            cout << endl << "grado 3 por lo que debe proveer 2 polinomios de 3 elementos y la operacion" << endl;
            cout << "por lo tanto tiene que escribir 7 argumentos, ejemplo:" << endl << endl;
            cout << "     ./OUT 1 2 3 + 4 5 6" << endl;
            cout << endl << "Lo que equivale a:" << endl << endl;
            cout << "     (1x^2 + 2x + 3) + (4x^2 + 5x + 6)" << endl;
            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return;
        }
        else if( verificar.Verifica_introdujeron_numeros(ARG, ARGV) == 1 ){
            cout << endl << "Hay un cero en el denominador =/" << endl;
            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return;
        }
        else 
            if ( verificar.Verifica_introdujeron_numeros(ARG, ARGV) == 2 ){}
            else {
                cout << endl << "ERROR en el codigo" << endl;
                cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
                return;
            }


        if( verificar.Verifica_int_float(ARG, ARGV) == 2){
            cout << endl << "Ponga enteros please :/" << endl;
            cout << endl;
            cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
            return;
        }

        
        ///< Verifica si se trata de un polinomio
        if( ARGV[2][0] != '/' && ARGV[6][0] != '/' )
        {

            x1  = atof( ARGV[1] );
            x12 = atof( ARGV[2] );
            x13 = atof( ARGV[3] );
            y1  = atof( ARGV[5] );
            y12 = atof( ARGV[6] );
            y13 = atof( ARGV[7] );
            
            pol1 = new Polinomio (x1, x12, x13);
            pol2 = new Polinomio (y1, y12, y13);
            
            if(ARGV[4][0] == '+'){

                (*pol1) + (*pol2);
                cout << *pol1;
            }
            else if(ARGV[4][0] == '-'){

                (*pol1) - (*pol2);
                cout << *pol1;
            }
            else if(ARGV[4][0] == '*'){

                (*pol1) * (*pol2);
                cout << *pol1;
            }
            else if(ARGV[4][0] == '/'){

                (*pol1) / (*pol2);
                cout << *pol1;
            }
            else cout << endl << "Esa operacion no existe :(";

            delete pol1;
            delete pol2;

        } //Fin de if
        else{
            
            switch(ARG)
            {
                case 4: 

                    //< Verifica si se introdujo un float.
                    if( verificar.Verifica_int_float (ARG, ARGV) == 1)
                    {
                        x2 = atof(ARGV[1]);
                        y2 = atof(ARGV[3]);

                        if     ( ARGV[2][0] == '+' ) x2+y2;
                        else if( ARGV[2][0] == '-' ) x2-y2;
                        else if( ARGV[2][0] == '*' ) x2*y2;
                        else if( ARGV[2][0] == '/' ) x2/y2;
                        else{
                            cout << endl << "Esa operacion no existe =(" ;
                            cout << endl;
                            break;
                        }

                        cout << endl << "Resultado: " << x2 << endl;
                        
                        break;
                    }//Fin de if

                    else 
                    {
                        cout << endl << "ERROR" << endl;
                        return;
                    }
                                   

                case 8: /** FRACCIONES */

                    //< Verifica si se introdujo un int.
                    if( verificar.Verifica_int_float(ARG, ARGV) == 1 )
                    {

                        if(ARGV[7] == to_string(0) || ARGV[3] == to_string(0))
                        {
                            cout << endl << "Hay un cero en el denominador =/" << endl;
                            cout << endl << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
                            return;
                        }

                        x2 = atof(ARGV[1]);
                        y2 = atof(ARGV[3]);
                        w2 = atof(ARGV[5]);
                        z2 = atof(ARGV[7]);

                        frac  = new Fraccion (x2, y2);
                        frac2 = new Fraccion (w2, z2);

                        if( ARGV[2][0] == '/' && ARGV[6][0] == '/' )
                        {
                            if( ARGV[4][0] == '+' ) {
                                
                                (*frac)+(*frac2) ; 
                                cout << "Resultado: " << *frac;  
                            }
                            else if( ARGV[4][0] == '-' ) {

                                (*frac)-(*frac2) ; 
                                cout << endl;
                                cout << "Resultado: " << *frac;
                            }
                            else if( ARGV[4][0] == '*' ) {

                                (*frac)*(*frac2) ; 
                                cout << endl;
                                cout << "Resultado: " << *frac;
                            }
                            else if( ARGV[4][0] == '/' ) {

                                (*frac)/(*frac2); 
                                cout << endl; 
                                cout << "Resultado: "<< *frac;
                            }
                            else{
                                cout << endl << "Esa operacion no existe =(" ;
                                cout << endl;
                                delete frac;
                                delete frac2;
                                break;
                            }
                        }
                        else{
                            cout << endl;
                            cout << "Induzca el simbolo / para las fracciones =)";
                            cout << endl;
                            delete frac;
                            delete frac2;
                            break;
                        }

                        delete frac;
                        delete frac2;
                        break;
                    }//Fin de if

                default:
                    cout << endl;
                    cout << "El programa funciona desde la linea de comandos";
                    cout << ", si quiere hacer operaciones con numeros enteros o reales";
                    cout << " escriba" << endl << "       ./OUT A + B " << endl;
                    cout << "Donde + puede ser tambien:" << endl;
                    cout << "       -" << endl << "       /" << endl << "       /*" << endl;
                    cout << endl << "Note que los espacios deben ser tal como se muestran de lo contrario";
                    cout << " el programa no funcionara como debe, tambien tenga en cuenta que el operador";
                    cout << " * solo se puede escribir de la siguiente forma '*'.";
                    cout << endl << endl;
                    cout << "Si quiere hacer operaciones en fracciones escriba:" << endl;
                    cout << "       ./OUT A / B + C / D" << endl;
                    cout << "Respetando los espacios y la operacion puede ser otra (resta, multiplicacion o division).";
                    break;

            } //Fin de switch
            
        }//Fin del else

    }//Fin de if
    else{
        cout << endl;
        cout << "El programa funciona desde la linea de comandos";
        cout << ", si quiere hacer operaciones con numeros enteros o reales";
        cout << " escriba" << endl << "       ./OUT A + B " << endl;
        cout << "Donde + puede ser tambien:" << endl;
        cout << "  -" << endl << "  /" << endl << "  /*" << endl;
        cout << endl << "Note que los espacios deben ser tal como se muestran de lo contrario";
        cout << " el programa no funcionara como debe, tambien tenga en cuenta que el operador";
        cout << " * solo se puede escribir de la siguiente forma '*'";
    }

    cout << endl << endl;
    cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
    
} //Fin de init


Control::~Control(){}