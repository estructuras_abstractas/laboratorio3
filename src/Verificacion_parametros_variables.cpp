/**
 * @date 22/01/2020
 *
 * @file Verificacion_parametros_variables.cpp
 * @brief Contiene los algoritmos y estructuras de control que describen 
 * el funcionamiento de la clase Verificacion_parametros_variables.
 */

#include "../include/Verificacion_parametros_variables.h"

using namespace std;


Verificacion_parametros_variables::Verificacion_parametros_variables(){}


Verificacion_parametros_variables::~Verificacion_parametros_variables(){}


unsigned short int Verificacion_parametros_variables::Verifica_int_float( int A, char **C){

    int entero, entero2, entero3, entero4;
    float real, real2, real3, real4;

    /**0 float
     1 int
     2 error
    */
    if (A == 4){
        real = atof(C[1]);
        entero = real;

        if(real - entero > 0){
            real = atof(C[3]);
            entero = real;

            if(real - entero > 0) return 0;
            else if(real - entero == 0) return 2;

        }
        else if (real - entero == 0){
            real = atof(C[3]);
            entero = real;

            if(real - entero > 0) return 2;
            else if(real - entero == 0) return 1;
        }
    }//Fin de if

    if (A == 8){
        real = atof(C[1]);
        entero = real;

        real2 = atof(C[3]);
        entero2 = real2;

        real3 = atof(C[5]);
        entero3 = real3;

        real4 = atof(C[7]);
        entero4 = real4;

        if( real - entero > 0 && real2 - entero2 > 0 && real3 - entero3 > 0 && real4 - entero4 > 0 ) return 0;
        else if( real - entero == 0 && real2 - entero2 == 0 && real3 - entero3 == 0 && real4 - entero4 == 0 ) return 1;
        else return 2;

    }//Fin de if

};


unsigned short int Verificacion_parametros_variables::Verifica_introdujeron_numeros( int A, char **C){

    //Regresa 0 si algun elemento es incorrecto
    //Regresa 2 si todo salio bien

    ///< Se está operando con fracciones.
    if(A == 8){ ///< Verifica si se ingresaron 7 elementos (uno de ellos es el nombre del programa).
        
        if( atof(C[1]) ){} ///< Se compara el parámetro para saber si se trata de un 0 o un número real.
        else return 0;


        if( atof(C[2]) || C[2][0]=='/' ) {} ///< Determina si el argumento es un /
        else return 0;


        if( atof(C[3]) || C[3] == to_string(0)){}
        else return 0;


        if( C[4][0] == '+' || C[4][0] == '-' || C[4][0] == '*' || C[4][0] == '/'){} ///< Verifica que el argumento sea alguna operación válida
        else return 0;


        if( atof(C[5]) ){}
        else return 0;


        if( atof(C[6]) || C[6][0]=='/' ){}
        else return 0;


        if( atof(C[7]) || C[7] == to_string(0) ){}
        else return 0;
        
        return 2; ///< Todo salió bien.       
    } //Fin de if
    
};