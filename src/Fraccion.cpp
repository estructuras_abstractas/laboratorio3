/**
 * @date 22/01/2020
 *
 * @file Fraccion.cpp
 * @brief En este archivo se definen los métodos de la clase Fraccion.
 */

#include "../include/Fraccion.h"


Fraccion::Fraccion(int X, int Y){
    x = X;
    y = Y;

    return;
}


Fraccion::~Fraccion(){}



void Fraccion::operator+(const Fraccion &obj){
    
    float fraccion1;
    float fraccion2;

    fraccion1 = (float)x/ (float)y;
    fraccion2 = (float)obj.x / (float)obj.y;
    
    resultado = fraccion1 + fraccion2;

    return;
}


void Fraccion::operator-(const Fraccion &obj){

    float fraccion1;
    float fraccion2;

    fraccion1 = (float)x/ (float)y;
    fraccion2 = (float)obj.x / (float)obj.y;
    
    resultado = fraccion1 - fraccion2;

    return;
}


void Fraccion::operator*(const Fraccion &obj){
    
    float fraccion1;
    float fraccion2;

    fraccion1 = (float)x/ (float)y;
    fraccion2 = (float)obj.x / (float)obj.y;
    
    resultado = fraccion1 * fraccion2;

    return;
}


void Fraccion::operator/(const Fraccion &obj){

    float fraccion1;
    float fraccion2;

    fraccion1 = (float)x/ (float)y;
    fraccion2 = (float)obj.x / (float)obj.y;
    
    resultado = fraccion1 / fraccion2;

    return;
}


ostream& operator << ( ostream& os, const Fraccion& obj)
{
    os << obj.resultado;
    return os;
}


istream& operator >> ( istream& is, Fraccion& obj)
{
    is >> obj.x >> obj.y;

    return is;
}