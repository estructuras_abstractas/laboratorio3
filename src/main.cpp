/**
 * @date 22/01/2020
 *
 * @file main.cpp
 * @brief Se inicializa la función principal (Control) del programa.
 */

#include <iostream>

#include "../include/Control.h"
#include "../include/Fraccion.h"
#include "../include/Polinomio.h"


using namespace std;

/** 
 * @fn main
 * @brief La función recibe los argumentos por consola y se encarga de ejecutar la función _init_ que da inicio al programa.
 */ 

int main(int argc, char *argv[]){

    ///< Funcion que da inicio al programa
    Control calculadora (argc, argv);

    return 0;

} //Fin de main